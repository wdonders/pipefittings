# Pipe fittings
`pipefittings` is a package that allows users to create data pipelines.
Out of the box, it offers:
- Reuse of pipeline outcomes
- Automatic DAG compilation
- Automatic logging
- Ability to diverge
- Ability to converge

**NOTE**: pipefittings is currently in an alpha phase and may break at any moment.

# Concept
## A fitting
A data pipeline that results in one or more `DataFrame`s consists of one or more `Fitting`s.
Each fitting can `process()` an initial DataFrame by `pipe`-ing the DataFrame through steps consisting of pure functions that take a `DataFrame` and return a (processed) `DataFrame`. An example step function is given by `exampel_processing_function`:

```python
def example_processing_function(df: DataFrame) -> DataFrame:
    """Example processing function to be used in Fittings. 
    This function does nothing and simply returns the input dataframe"""
    return df
````

This function is added as a staticmethod to the `Fitting` subclass and registered as a step in the `Fitting` `steps` class variable. 

```python
class NotAllowedFitting(Fitting):
    """Note: does not work; Fitting class requires a `initial` class variable."""

    @staticmethod
    def example_processing_function(df: DataFrame) -> DataFrame:
        """Example processing function to be used in Fittings. 
        This function does nothing and simply returns the input dataframe"""
        return df

    steps = [
        example_processing_function
    ] 
```

An initial dataframe is determined from the `set_initial` function that must be implemented:

```python
class InitialFitting(Fitting):
    """This is well-defined Fitting that you can call .process() on."""
    initial = None

    def set_initial(self):
        """This method is called when the class variable `initial` is set to None"""
        return pd.DataFrame()

    @staticmethod
    def example_processing_function(df: DataFrame) -> DataFrame:
        """Example processing function to be used in Fittings. 
        This function does nothing and simply returns the input dataframe"""
        return df

    steps = [
        example_processing_function
    ] 
```

## Creating a pipeline from fittings
`Fitting`s can be connected in three ways:

* Linear:`Fitting 2` linearly follows from `Fitting 1`
* Diverging: `Fitting 2` diverges into `Fitting 3` and `Fitting 4`
* Converging: `Fitting 6` is the convergence of `Fitting 5` and `Fitting 4`

## Linear pipeline
Let's define a new `Fitting` subclass that takes as the initial DataFrame the output of the `InitialFitting` `Fitting`:

```python
class NextFitting(Fitting):
    """No-op fitting"""
    initial = InitialFitting
    steps = []
```

This results in the following pipeline:

```mermaid
graph LR
    subgraph Linear pipeline
        direction RL
        A(Input DataFrame) -->|initial| B[InitialFitting]
        B -->|".process()"| C[NextFitting]
        C --> D(Output DataFrame)
    end
```

To obtain the `Output DataFrame`, it is sufficient to instantiate a `NextFitting` instance and call `.process` on it:
```bash
>>> output_dataframe = NextFitting().process()
INFO     | Processing InitialFitting pipeline
INFO     |  * Step 0: This method is called when the class variable `initial` is set to None
INFO     | Processing NextFitting pipeline
```

## Diverging pipeline
A diverging pipeline is created by defining two `Fitting` subclasses that both use the same `Fitting` subclass as their `initial` DataFrame:
```python
class Top(Fitting):
    initial = InitialFitting
    steps = []

class Bottom(Fitting):
    initial = InitialFitting
    steps = []
```

This results in the following pipeline:

```mermaid
graph LR
subgraph Diverging pipeline
    A(Input DataFrame) -->|initial| B[InitialFitting]
    B -->|".process()"| C[TopFitting]
    B -->|".process()"| D[BottomFitting]
    C -->|".process()"| E(Output DataFrame 1)
    D -->|".process()"| F(Output DataFrame 2)
end
```
To obtain the `Output DataFrame 1`, it is sufficient to instantiate a `TopFitting` instance and call `.process` on it:
```bash
>>> output_dataframe = TopFitting().process()
INFO     | Processing InitialFitting pipeline
INFO     |  * Step 0: This method is called when the class variable `initial` is set to None
INFO     | Processing TopFitting pipeline
```

To obtain `Output DataFrame 2`, instantiate a `BottomFitting` instance and call `.process()` on it:
```bash
>>> output_dataframe = NextFitting().process()
INFO     | Returning cached InitialFitting pipeline result
INFO     | Processing Bottomfitting pipeline
```
Note that in the first line of the logs, a cached result is used.

## Convering pipeline
A converging pipeline is created by defining a `Fitting` subclasses that sets both the `initial` and the `other` class variable.
```python
class CombineFitting(Fitting):
    initial = TopFitting
    other = BottomFitting
    steps = []
```

This results in the following pipeline:

```mermaid
graph LR
subgraph Diverging pipeline
    A(Input DataFrame) -->|initial| B[InitialFitting]
    B -->|".process()"| C[TopFitting]
    B -->|".process()"| D[BottomFitting]
    C -->|".process()"| E[CombineFitting]
    D -->|".process()"| E[CombineFitting]
    E -->|".process()"| F(Output DataFrame)
end

To obtain `Output DataFrame`, instantiate a `CombineFitting` instance and call `process()` on it:

```bash
>>> output_dataframe = TopFitting().process()
INFO     | Processing InitialFitting pipeline
INFO     |  * Step 0: This method is called when the class variable `initial` is set to None
INFO     | Processing TopFitting pipeline
INFO     | Processing BottomFitting pipeline
INFO     | Processing CombineFitting pipeline
```

## Complex pipeline
A complex Pipeline is shown in the flowchart below.

```mermaid
graph LR
    subgraph Pipeline
        direction RL
        A(Input DataFrame) -->|initial| B[Fitting 1]
        B -->|".process()"| C[Fitting 2]
        C -->|".process()"| D[Fitting 3]
        C -->|".process()"| E[Fitting 4]
        D -->|".process()"| F[Fitting 5]
        F -->|".process()"| I(Output DataFrame 1)
        F -->|".process()"| G[Fitting 6]
        E -->|".process()"| G
        G -->|".process()"| H(Output DataFrame 2)
    end
```

# Example

```python
from pipefittings import Fitting
import pandas as pd


class Data(Fitting):
    initial = None

    def set_initial(self):
        return pd.DataFrame()

    @staticmethod
    def add_column(df: pd.DataFrame):
        """Add a column"""
        return df.assign(new_column=None)

    @staticmethod
    def set_data(df: pd.DataFrame):
        """Set data for column"""
        df["new_column"] = [1, 2, 3]
        return  df

    steps = [
        add_column,
        set_data,
    ]

Data().processed.info()
```
Returns:
```
INFO     | Processing Data pipeline
INFO     |  * Step 0: Add a column
DEBUG    |  * Log: Log(time_spent=0.001053839998348849, delta_rows=0, delta_columns=1, columns_added=['new_column'], columns_removed=[])
INFO     |  * Step 1: Set data for column
DEBUG    |  * Log: Log(time_spent=0.0012215349997859448, delta_rows=0, delta_columns=0, columns_added=[], columns_removed=[])
<class 'pandas.core.frame.DataFrame'>
RangeIndex: 3 entries, 0 to 2
Data columns (total 1 columns):
 #   Column      Non-Null Count  Dtype
---  ------      --------------  -----
 0   new_column  3 non-null      int64
dtypes: int64(1)
memory usage: 152.0 bytes
```

# Future work

- Support `ibis-framework` to work with all kinds of backends, not just pandas
