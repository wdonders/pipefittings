from __future__ import annotations

from functools import partial
from time import perf_counter
from typing import Callable

from pandas import DataFrame

from .logs import Log


class Step:
    """Pipeline step"""

    def __init__(self, step: Callable | tuple[Callable, dict]) -> None:
        if isinstance(step, tuple):
            func, kwargs = step
            self.func = partial(func, **kwargs)

            # Update `qualname` due to partial application
            self.func.__qualname__ = func.__qualname__  # type: ignore
            self.func.__doc__ = func.__doc__

        else:
            func, kwargs = step, {}
            self.func = func  # type: ignore

        self.kwargs = kwargs
        self.doc = func.__doc__

    def apply(self, df: DataFrame) -> tuple[DataFrame, Log]:
        """Log the result"""
        self.initial = df
        tic = perf_counter()
        self.result = self.func(df)
        toc = perf_counter()
        return self.result, Log(
            delta_rows=self.delta_rows,
            delta_columns=self.delta_columns,
            columns_added=self.columns_added,
            columns_removed=self.columns_removed,
            time_spent=toc - tic,
        )

    @property
    def delta_rows(self) -> int:
        """Return the delta in rows"""
        return self.result.shape[0] - self.initial.shape[0]

    @property
    def delta_columns(self) -> int:
        """Return the delta in columns"""
        return self.result.shape[1] - self.initial.shape[1]

    @property
    def columns_added(self) -> list[str]:
        return [col for col in self.result.columns if col not in self.initial.columns]

    @property
    def columns_removed(self) -> list[str]:
        return [col for col in self.initial.columns if col not in self.result.columns]

    def __str__(self):
        return self.func.__qualname__

    def __repr__(self):
        if self.doc is None:
            return f"Step({str(self)}: (missing docstring)"
        return f"Step({str(self)}: {' '.join(docline.strip() for docline in self.doc.splitlines())})"
