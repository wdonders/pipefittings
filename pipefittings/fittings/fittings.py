import sys

from pandas import DataFrame

from .logs import level_filter, log
from .steps import Step


class Fitting:
    """Parent class for all fitting classes"""

    def __new__(cls):
        """Create a new instance only if cls.instance does not yet exist (singleton pattern)"""
        if not hasattr(cls, "steps"):
            cls.steps = []
        if not hasattr(cls, "instance"):
            cls.instance = super(Fitting, cls).__new__(cls)
        return cls.instance

    def __init__(self):
        """Set the initial dataframe"""
        initial_class = getattr(self.__class__, "initial", None)
        if initial_class is None:
            if not hasattr(self, "set_initial"):
                raise AttributeError(
                    f"{self.__class__.__name__} must implement 'set_initial' method if `initial` class variable is `None`"
                )
            self.initial = self.set_initial()
        else:
            self.initial = initial_class().processed

        other_class = getattr(self.__class__, "other", None)
        if other_class is not None:
            self.other = other_class().processed
        else:
            self.other = None

        self.pipeline = [Step(step) for step in self.steps]
        self.logs = []
        self.summary = {
            "delta_rows": 0,
            "delta_columns": 0,
            "added_columns": [],
            "removed_columns": [],
            "time_spent": 0,
        }

    def process(self, info: bool = False, debug: bool = False) -> None:
        """Perform steps on the initial dataframe and return the processed dataframe.
        If `info=True`, output logging messages with level INFO
        if `debug=True`, output logging messages with level DEBUG"""
        if info:
            info_id = log.add(sys.stderr, filter=level_filter(level="INFO"))

        if debug:
            debug_id = log.add(sys.stderr, filter=level_filter(level="DEBUG"))
        log.info(f"Processing '{self.__class__.__name__}'")

        df = self.initial

        # Reset logs
        self.logs = []

        for num, step in enumerate(self.pipeline):
            df, step_log = step.apply(df)
            log.info(
                f" * Step {num}: {'Missing docstring' if not hasattr(step.func, '__doc__') else step.func.__doc__}"
            )
            log.debug(f" * Log: {step_log}")
            self.logs.append(step_log)
        self._processed = df

        if info:
            log.remove(info_id)

        if debug:
            log.remove(debug_id)

    @property
    def processed(self) -> DataFrame:
        """Return the result from self.process() and store it as the `processed` attribute"""
        if not hasattr(self, "_processed"):
            self.process()
        return self._processed

    def __str__(self):
        return f"{self.__class__.__name__}(initial={'None' if (initial := getattr(self.__class__, 'initial', None)) is None else initial.__name__}, other={'None' if (other := getattr(self.__class__, 'other', None)) is None else other.__name__}, steps=({len(self.pipeline)}): {self.pipeline})"  # noqa: E501

    def __iter__(self):
        """Implement iteration through steps"""
        # Reset logs
        self.logs = []

        # Reset df
        df = self.initial

        # Iterate
        for num, step in enumerate(self.pipeline):
            df, step_log = step.apply(df)
            log.info(
                f" * Step {num}: {'Missing docstring' if not hasattr(step.func, '__doc__') else step.func.__doc__}"
            )
            log.debug(f" * Log: {step_log}")
            self.logs.append(step_log)
            yield df

        self._processed = df

    def __repr__(self):
        return self.__str__()
