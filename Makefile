.dummy: test build devenv

.ONESHELL:

.SHELLFLAGS := -euo pipefail -c
SHELL := bash

venv:
	python3.10 -m venv venv
	source venv/bin/activate
	pip install -r requirements-dev.txt

devenv: venv
	source venv/bin/activate
	mypy --install-types

build:
	flit build

test: build
	tox -p
