import pytest
from pandas import DataFrame
from pandas.testing import assert_frame_equal

from pipefittings import Fitting


def test_fitting_missing_steps_raises_attributeerror():
    """Test whether missing `set_initial` raises AttributeError"""

    class Test(Fitting):
        initial = None

    with pytest.raises(AttributeError):
        Test()


def test_fitting_missing_steps():
    """Test whether missing `steps` raises AttributeError"""

    class Test(Fitting):
        initial = None

        def set_initial(self):
            return None

    Test()


def test_noop_fitting(NoOpFitting):
    """Test whether Fitting is allowed"""
    assert isinstance(NoOpFitting(), Fitting)


def test_noop_fitting_no_steps_returns_initial(NoOpFitting):
    """Test whether calling `processed` on the Fitting returns the initial DataFrame if no steps are provided"""
    assert_frame_equal(NoOpFitting().set_initial(), NoOpFitting().processed)


def test_noop_fitting_is_singleton(NoOpFitting):
    """Test whether a Fitting is singleton"""
    a = NoOpFitting()
    b = NoOpFitting()
    assert a is b


def test_connected_fitting(NoOpFitting):
    """Test connection of NoOpFitting -> First with no steps returns initial dataframe"""

    class First(Fitting):
        initial = NoOpFitting
        steps = []

    assert isinstance(First(), Fitting)
    assert_frame_equal(NoOpFitting().set_initial(), First().processed)


def test_multiple_connected_fittings(NoOpFitting):
    """Test connection of NoOpFitting -> First -> Second with no steps returns initial dataframe"""

    class First(Fitting):
        initial = NoOpFitting
        steps = []

    class Second(Fitting):
        initial = First
        steps = []

    assert isinstance(Second(), Fitting)
    assert_frame_equal(NoOpFitting().set_initial(), First().processed)
    assert_frame_equal(NoOpFitting().set_initial(), Second().processed)


def test_diverging_fittings(NoOpFitting):
    """Test whether divergence of fittings returns the initial dataframe downstream"""

    class Left(Fitting):
        initial = NoOpFitting
        steps = []

    class Right(Fitting):
        initial = NoOpFitting
        steps = []

    assert_frame_equal(NoOpFitting().set_initial(), Left().processed)
    assert_frame_equal(NoOpFitting().set_initial(), Right().processed)
    assert_frame_equal(Left().processed, Right().processed)


def test_converging_fittings(NoOpFitting):
    """Test whether convergence of fittings returns initial DataFrame when no steps are passed"""

    class Convergence(Fitting):
        initial = NoOpFitting
        other = NoOpFitting
        steps = []

    assert_frame_equal(NoOpFitting().set_initial(), Convergence().processed)


def test_noop_step_returns_initial(InitialFitting, step_noop):
    """Test whether Fitting with no-op step returns initial"""

    class Test(Fitting):
        initial = InitialFitting
        steps = [step_noop]

    assert_frame_equal(InitialFitting().processed, Test().processed)


def test_multiple_noop_step_returns_initial(InitialFitting, step_noop):
    """Test whether Fitting with multiple no-op steps returns initial"""

    class Test(Fitting):
        initial = InitialFitting
        steps = [
            step_noop,
            step_noop,
        ]

    assert_frame_equal(InitialFitting().processed, Test().processed)


def test_step_drop_columns(InitialFitting, step_drop_columns):
    class Example(Fitting):
        initial = InitialFitting
        steps = [
            (step_drop_columns, {"columns": ["int_column"]}),
        ]

    assert "int_column" not in Example().processed.columns


def test_step_keep_columns(InitialFitting, step_keep_columns):
    class Example(Fitting):
        initial = InitialFitting
        steps = [
            (step_keep_columns, {"columns": ["int_column", "bool_column"]}),
        ]

    assert Example().processed.shape[1] == 2
    assert "int_column" in Example().processed.columns
    assert "bool_column" in Example().processed.columns


def test_iteration(InitialFitting, step_keep_columns):
    """Test whether we can iterate over the steps"""

    class Example(Fitting):
        initial = InitialFitting
        steps = [
            (step_keep_columns, {"columns": ["int_column", "bool_column"]}),
            (step_keep_columns, {"columns": ["int_column", "bool_column"]}),
            (step_keep_columns, {"columns": ["int_column", "bool_column"]}),
        ]

    example = Example()
    for i, df in enumerate(iter(example)):
        isinstance(df, DataFrame)

    assert i + 1 == len(Example.steps)
