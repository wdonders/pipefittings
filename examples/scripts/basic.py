import pandas as pd

from pipefittings import Fitting


class Data(Fitting):
    initial = None

    def set_initial(self):
        return pd.DataFrame()

    @staticmethod
    def add_column(df: pd.DataFrame):
        """Add a column"""
        return df.assign(new_column=None)

    @staticmethod
    def set_data(df: pd.DataFrame):
        """Set data for column"""
        df["new_column"] = [1, 2, 3]
        return df

    steps = [
        add_column,
        set_data,
    ]


Data().processed.info()
