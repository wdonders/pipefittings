-r requirements.txt
bump2version==1.0.1
flit==3.8.0
flit_core==3.8.0
pre-commit==2.20.0
pytest==7.2.0
tox==3.27.1
twine==4.0.1
