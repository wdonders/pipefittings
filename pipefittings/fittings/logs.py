import sys
from dataclasses import dataclass

from loguru import logger as log

DEFAULT_LEVEL = "DEBUG"


def level_filter(level):
    def is_level(record):
        return record["level"].name == level

    return is_level


log.remove()
fmt = "{level: <8} | {message}"
log.add(sink=sys.stderr, colorize=True, level=DEFAULT_LEVEL, format=fmt)


@dataclass
class Log:
    time_spent: float
    delta_rows: int
    delta_columns: int
    columns_added: list[str]
    columns_removed: list[str]
