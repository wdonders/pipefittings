import datetime

import pandas as pd
import pytest
from pandas import DataFrame

from pipefittings import Fitting


@pytest.fixture
def example() -> pd.DataFrame:
    """Return pandas DataFrame created from sample data"""
    return pd.read_csv("tests/fixtures/example.csv", sep=";")


@pytest.fixture(
    scope="function"
)  # scope: function requires, because Fitting is singleton!
def NoOpFitting(example) -> type:
    class NoOp(Fitting):
        initial = None

        def set_initial(self):
            return pd.DataFrame()

    return NoOp


@pytest.fixture(scope="function")
def InitialFitting(NoOpFitting) -> type:
    class Initial(Fitting):
        initial = None

        def set_initial(self):
            return pd.DataFrame(
                {
                    "int_column": [-1, 0, 1],
                    "float_column": [-1.0, 0.0, 1.0],
                    "bool_column": [True, False, True],
                    "string_column": ["a", "b", "c"],
                    "datetime_column": [
                        datetime.datetime.now(),
                        datetime.datetime.now(),
                        datetime.datetime.now(),
                    ],
                }
            )

    return Initial


@pytest.fixture
def step_noop():
    """A step function that does nothing"""

    def step(df: DataFrame):
        return df

    return step


@pytest.fixture
def step_noop_arg():
    """A step function with a keyword argument that does nothing"""

    def step(df, *, arg):
        """Do nothing"""
        return df

    return step


@pytest.fixture
def step_return_one():
    """Step function for returning just the first record"""

    def step(df: DataFrame):
        """Return first record"""
        return df.head(1)

    return step


@pytest.fixture
def step_drop_columns():
    """Step function to drop particular columns"""

    def step(df: DataFrame, columns: list[str]):
        """Drop columns"""
        return df.drop(columns=columns)

    return step


@pytest.fixture
def step_keep_columns():
    """Step function to keep particular columns"""

    def step(df: DataFrame, columns: list[str]):
        """Keep columns"""
        drop_columns = [column for column in df.columns if column not in columns]
        return df.drop(columns=drop_columns)

    return step
