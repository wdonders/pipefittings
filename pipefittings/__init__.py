"""The pipefittings package"""
__version__ = "0.3.0"

from .fittings import Fitting  # noqa: F401
